#define FF17 &FreeSans9pt7b
#define FF18 &FreeSans12pt7b
#define FF19 &FreeSans18pt7b
#define FF20 &FreeSans24pt7b

#define FF21 &FreeSansBold9pt7b
#define FF22 &FreeSansBold12pt7b
#define FF23 &FreeSansBold18pt7b
#define FF24 &FreeSansBold24pt7b

#include "NotoSansBold15.h"
#include "NotoSansBold36.h"

#define AA_FONT_SMALL NotoSansBold15
#define AA_FONT_LARGE NotoSansBold36

#define SDA 21
#define SCL 22

#define RX_PIN 27                                          // Rx pin which the MHZ19 Tx pin is attached to
#define TX_PIN 26                                          // Tx pin which the MHZ19 Rx pin is attached to
#define BAUDRATE 9600                                      // Device to MH-Z19 Serial baudrate (should not be changed)


#define BUTTON_1            35
#define BUTTON_2            0


#define WIFI_MQTT_ON
const char*   WIFI_SSID       = "";
const char*   WIFI_PASSWORD   = "";

// Adafruit HUZZAH32 ESP32 Feather Battery is connected to ADC pin A13 (or 35)
#define ADC_EN              14  //ADC_EN is the ADC detection enable port
#define ADC_PIN             34


// MQTT topic gets defined by "<MQTT_BASE_TOPIC>/<MAC_ADDRESS>/<property>"
// where MAC_ADDRESS is one of the values from FLORA_DEVICES array
// property is either temperature, moisture, conductivity, light or battery

const char*   MQTT_HOST       = "homeassistant";
const int     MQTT_PORT       = 1883;
const char*   MQTT_CLIENTID   = "co2-sensor";
const char*   MQTT_USERNAME   = "";
const char*   MQTT_PASSWORD   = "";
const String  MQTT_BASE_TOPIC = "/co2"; 
const int     MQTT_RETRY_WAIT = 5000;
