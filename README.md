# CO2 Sensor

![CO2 Sensor](case/co2_sensor.jpg "CO2 Sensor")

An ESP32-based project that displays
- CO2 (parts per million)
- TVOC (parts per million)
- Relative humidity
- Temperature

Also optionally submits values periodically to MQTT via WIFI.

## Hardware
- TTGO T Display: https://www.aliexpress.com/item/33048962331.html
- MH-Z19E CO2 Sensor: https://www.aliexpress.com/item/1005003233738268.html
- SGP30 TVOC Sensor: https://www.aliexpress.com/item/4000004614708.html
- AM2322 Temperature / CO2 Sensor: https://www.aliexpress.com/item/32319589688.html

## License
GPL v3
