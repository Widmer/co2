#include <SPI.h>
#include <Wire.h>
#include <Adafruit_SGP30.h>
#include <MHZ19.h>  
#include <AM232X.h>
#include <TFT_eSPI.h> // Hardware-specific library
#include <PubSubClient.h>
#include <WiFi.h>
#include "Button2.h"

#include "config.h"

AM232X AM2322;
Adafruit_SGP30 sgp;



MHZ19 myMHZ19;                                             // Constructor for library
HardwareSerial mySerial(2);                   // (Uno example) create device to MH-Z19 serial


TFT_eSPI tft = TFT_eSPI(135, 240); // Invoke custom library
TFT_eSprite spr = TFT_eSprite(&tft); // Sprite class needs to be invoked

Button2 btn1(BUTTON_1);
Button2 btn2(BUTTON_2);


#define TFT_GREY 0x5AEB

uint32_t updateTime = 0;       // time for next update

bool AM2322_success = false;
float AM2322_humidity = -INFINITY ; 
float AM2322_temperature = -INFINITY ;

bool sgp_success = false;
uint16_t sgp_TVOC = 0;
uint16_t sgp_eCO2 = 0;
int page_number = 0;

const int16_t  mid_offset = 120;

bool mhz19_success = false;
int mhz19_co2 = -1;
int8_t mhz19_temperature = -100; 

char buffer[40];
char buffer2[40];

char macAddr[18];

WiFiClient espClient;
PubSubClient client(espClient);

SemaphoreHandle_t xMutex;

uint32_t getAbsoluteHumidity(float temperature, float humidity) 
{
    // approximation formula from Sensirion SGP30 Driver Integration chapter 3.15
    const float absoluteHumidity = 216.7f * ((humidity / 100.0f) * 6.112f * exp((17.62f * temperature) / (243.12f + temperature)) / (273.15f + temperature)); // [g/m^3]
    const uint32_t absoluteHumidityScaled = static_cast<uint32_t>(1000.0f * absoluteHumidity); // [mg/m^3]
    return absoluteHumidityScaled;
}

void button_init()
{
  
    btn2.setLongClickHandler([](Button2 & b) {
      Serial.println("Bottom button pressed long");
      spr.loadFont(AA_FONT_SMALL);
      myMHZ19.calibrateZero();
      tft.setCursor(0, 0);
      tft.fillScreen(TFT_BLACK);
      spr.setTextColor(TFT_WHITE, TFT_BLACK);
      
      spr.printToSprite("Starting CO2 sensor calibration");
      tft.setCursor(0, 20);
      spr.printToSprite("to ambient (outside) levels.");
      spr.unloadFont();
      delay(5000);
      tft.fillScreen(TFT_BLACK);
    });
  
  btn2.setPressedHandler([](Button2 & b) {
    Serial.println("Bottom button pressed");
    
    tft.setCursor(0, 0);
    tft.fillScreen(TFT_BLACK);
    spr.loadFont(AA_FONT_SMALL);
    spr.setTextColor(TFT_WHITE, TFT_BLACK);

    int i = 50;
    while (i > 0) {
      tft.setCursor(0, 0);
      sprintf(buffer, "Press for %d seconds", (int)round((float)i / 10.0));
      spr.printToSprite(buffer);
      tft.setCursor(0, 20);
      spr.printToSprite("to calibrate CO2 sensor");
      tft.setCursor(0, 40);
      spr.printToSprite("to ambient (outside) levels.");
      
      if (digitalRead(0) != LOW) {
        break;
      }
      delay(100);
      i--;
    }

    while(digitalRead(0) == LOW) {
      delay(100);
    }
    spr.unloadFont();
    tft.fillScreen(TFT_BLACK);
  });

  
  btn1.setPressedHandler([](Button2 & b) {
    Serial.println("btn press wifi scan");
    tft.fillScreen(TFT_BLACK);
    page_number = (page_number + 1) % 3;
  });
  
}

void setup(void)
{
  Serial.begin(115200);
  while (!Serial) { delay(100); } // Wait for serial console to open!

  mySerial.begin(BAUDRATE, SERIAL_8N1, RX_PIN, TX_PIN, false, 1000);
  
  Serial.println("Sensor test");
  
  Wire.begin(SDA, SCL);
  
  Serial.print("AM232X LIBRARY VERSION: ");
  Serial.println(AM232X_LIB_VERSION);
  Serial.println();

  while (! AM2322.begin()) {
    Serial.println("AM2322 not found");
    delay(100);
  }
  
  
  while (! sgp.begin()){
    Serial.println("SGP30 not found :(");
    delay(100);
  }
  
  myMHZ19.begin(mySerial);

  myMHZ19.autoCalibration();                              // Turn auto calibration ON (OFF autoCalibration(false))

  button_init();
  tft.init();
  tft.setRotation(1);
  tft.fillScreen(TFT_BLACK);
  tft.setCursor(0, 0);
  tft.setTextDatum(BL_DATUM); 
  spr.setColorDepth(16); // 16 bit colour needed to show antialiased fonts


  xMutex = xSemaphoreCreateMutex();
#ifdef WIFI_MQTT_ON
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  byte ar[6];
  WiFi.macAddress(ar);
  sprintf(macAddr, "%02X:%02X:%02X:%02X:%02X:%02X", ar[0], ar[1], ar[2], ar[3], ar[4], ar[5]);  
  
  xTaskCreate(
    checkWifiAndSubmitMQTT,
    "WIFI MQTT",
    10000,
    NULL,
    1,
    NULL            // Task handle
  );
#endif
  
  updateTime = millis(); // Next update time
}

int counter = 0;
void loop() {
  if (updateTime <= millis()) {
    updateTime = millis() + 1000; // Delay to limit speed of update
    xSemaphoreTake( xMutex, portMAX_DELAY );

    Serial.print("AM2322, \t");
    int status = AM2322.read();
    switch (status)
    {
      case AM232X_OK:
        Serial.print("OK,\t");
        AM2322_success = true;
        AM2322_humidity = AM2322.getHumidity();
        AM2322_temperature = AM2322.getTemperature();

        Serial.print("Humidity: "); Serial.print(AM2322_humidity); Serial.print(" %");
        Serial.print("Temperature: "); Serial.print(AM2322_temperature); Serial.println(" °C");
      break;
      default:
        Serial.print(status);
        Serial.print("\t");
        AM2322_success = false;
      break;
    }
    
    sgp.setHumidity(getAbsoluteHumidity(AM2322_temperature, AM2322_humidity));
    delay(100);
    if (! sgp.IAQmeasure()) {
      Serial.println("SGP measurement failed");
      sgp_success = false;
      // return;
    } else {
      sgp_success = true;
      sgp_TVOC = sgp.TVOC;
      sgp_eCO2 = sgp.eCO2;
      Serial.print("TVOC "); Serial.print(sgp_TVOC); Serial.print(" ppb\t");
      Serial.print("eCO2 "); Serial.print(sgp_eCO2); Serial.println(" ppm");
    }


    /* note: getCO2() default is command "CO2 Unlimited". This returns the correct CO2 reading even 
    if below background CO2 levels or above range (useful to validate sensor). You can use the 
    usual documented command with getCO2(false) */

    mhz19_success = true;
    mhz19_co2 = myMHZ19.getCO2();                             // Request CO2 (as ppm)
    if (myMHZ19.errorCode != RESULT_OK) {
      mhz19_success = false;
      Serial.println("MH-Z19: Error found in communication");
    } else {
      Serial.print("MH-Z19C CO2 (ppm): ");                      
      Serial.println(mhz19_co2);      
    }
                      

    mhz19_temperature = myMHZ19.getTemperature();                     // Request Temperature (as Celsius)

    if (myMHZ19.errorCode != RESULT_OK) {
      mhz19_success = false;
      Serial.println("MH-Z19: Error found in communication");
    } else {
      Serial.print("MH-Z19C Temperature (C): ");                  
      Serial.println(mhz19_temperature);
    }        
    
    counter++;
    if (counter == 30) {
      counter = 0;

      uint16_t TVOC_base, eCO2_base;
      if (! sgp.getIAQBaseline(&eCO2_base, &TVOC_base)) {
          Serial.println("Failed to get baseline readings");
          return;
        }
      Serial.print("****Baseline values: eCO2: 0x"); Serial.print(eCO2_base, HEX);
      Serial.print(" & TVOC: 0x"); Serial.println(TVOC_base, HEX);
    }
    xSemaphoreGive( xMutex );
  }
  btn1.loop();
  btn2.loop();
  if (page_number == 0) {
    displayBigState();
  } else if (page_number == 1) {
    displayFullState();
  } else {
    displayHello();
  }
}
void displayHello() {
  spr.setTextColor(TFT_WHITE, TFT_BLACK);
  spr.loadFont(AA_FONT_LARGE);
  tft.setCursor(0, 0);
  spr.printToSprite("CO2 Sensor");
  spr.unloadFont();
  spr.loadFont(AA_FONT_SMALL);
  tft.setCursor(0, 40);
  spr.printToSprite("Code available at");
  tft.setCursor(0, 60);
  spr.printToSprite("gitlab.com/Widmer/co2");
  spr.unloadFont();
}

void displayBigState() {
  
  

  
  if (!mhz19_success) {
    spr.setTextColor(TFT_DARKGREY, TFT_BLACK);
  } else {
    if (mhz19_co2 > 1000) {
      if (mhz19_co2 > 2000) {
        spr.setTextColor(TFT_RED, TFT_BLACK);
      } else {
        spr.setTextColor(TFT_YELLOW, TFT_BLACK);
      }      
    } else {
      spr.setTextColor(TFT_WHITE, TFT_BLACK);
    }
  }
  tft.setCursor(0, 0);
  spr.loadFont(AA_FONT_SMALL);
  spr.printToSprite("CO2 ppm");
  spr.unloadFont();
  
  tft.setCursor(0, 20);
  spr.loadFont(AA_FONT_LARGE);
  sprintf(buffer, "%d  ", mhz19_co2);
  spr.printToSprite(buffer);
  spr.unloadFont();

  if (!sgp_success) {
    spr.setTextColor(TFT_DARKGREY, TFT_BLACK);
  } else {
    if (sgp_TVOC > 500) {
      if (sgp_TVOC > 1000) {
        spr.setTextColor(TFT_RED, TFT_BLACK);
      } else {
        spr.setTextColor(TFT_YELLOW, TFT_BLACK);
      }      
    } else {
      spr.setTextColor(TFT_WHITE, TFT_BLACK);
    }
  }
  tft.setCursor(mid_offset, 0);
  spr.loadFont(AA_FONT_SMALL);
  spr.printToSprite("TVOC ppm");
  spr.unloadFont();
  
  tft.setCursor(mid_offset, 20);
  spr.loadFont(AA_FONT_LARGE);
  sprintf(buffer, "%d  ", sgp_TVOC);
  spr.printToSprite(buffer); 
  spr.unloadFont();


  if (!AM2322_success) {
    spr.setTextColor(TFT_DARKGREY, TFT_BLACK);
  } else {
    spr.setTextColor(TFT_WHITE, TFT_BLACK);
  }

  
  spr.loadFont(AA_FONT_SMALL);
  tft.setCursor(0, 67);
  spr.printToSprite("Temp °C");
  tft.setCursor(mid_offset, 67);
  spr.printToSprite("Humidity %");
  spr.unloadFont();
  
  
  spr.loadFont(AA_FONT_LARGE);
  tft.setCursor(0, 87);
  sprintf(buffer, "%.1f  ", AM2322_temperature);
  spr.printToSprite(buffer);
  tft.setCursor(mid_offset, 87);
  sprintf(buffer, "%.1f  ", AM2322_humidity);
  spr.printToSprite(buffer); 
  spr.unloadFont();
  
}

void displayFullState() {
  spr.loadFont(AA_FONT_SMALL);
  tft.setCursor(0, 0);
  
  if (!AM2322_success) {
    spr.setTextColor(TFT_DARKGREY, TFT_BLACK);
  } else {
    spr.setTextColor(TFT_WHITE, TFT_BLACK);
  }
  
  sprintf(buffer, "Temp %.1f °C  ", AM2322_temperature);
  spr.printToSprite(buffer);
  tft.setCursor(mid_offset, 0);
  sprintf(buffer2, "RH %.1f %%  ", AM2322_humidity);
  spr.printToSprite(buffer2);
  
  
  
  
  if (!mhz19_success) {
    spr.setTextColor(TFT_DARKGREY, TFT_BLACK);
    sprintf(buffer, "CO2 %d ppm  ", mhz19_co2);
    tft.setCursor(0, 30); spr.printToSprite(buffer);
    
    sprintf(buffer2, "Temp %d °C  ", mhz19_temperature);
    tft.setCursor(mid_offset, 30); spr.printToSprite(buffer2);  
  } else {
    if (mhz19_co2 > 1000) {
      if (mhz19_co2 > 2000) {
        spr.setTextColor(TFT_RED, TFT_BLACK);
      } else {
        spr.setTextColor(TFT_YELLOW, TFT_BLACK);
      }      
    } else {
      spr.setTextColor(TFT_WHITE, TFT_BLACK);
    }
    sprintf(buffer, "CO2 %d ppm  ", mhz19_co2);
    tft.setCursor(0, 30); spr.printToSprite(buffer);
    spr.setTextColor(TFT_WHITE, TFT_BLACK);
    sprintf(buffer2, "Temp %d °C  ", mhz19_temperature);
    tft.setCursor(mid_offset, 30); spr.printToSprite(buffer2);  
  }

  
  if (!sgp_success) {
    spr.setTextColor(TFT_DARKGREY, TFT_BLACK);
    sprintf(buffer, "TVOC %d ppm  ", sgp_TVOC);
    sprintf(buffer2, "eCO2 %d ppm  ", sgp_eCO2);
    tft.setCursor(0, 60); spr.printToSprite(buffer);
    tft.setCursor(0, 80); spr.printToSprite(buffer2);  
  } else {
    if (sgp_TVOC > 500) {
      if (sgp_TVOC > 1000) {
        spr.setTextColor(TFT_RED, TFT_BLACK);
      } else {
        spr.setTextColor(TFT_YELLOW, TFT_BLACK);
      }      
    } else {
      spr.setTextColor(TFT_WHITE, TFT_BLACK);
    }
    sprintf(buffer, "TVOC %d ppm  ", sgp_TVOC);
    tft.setCursor(0, 60); spr.printToSprite(buffer);    

    if (sgp_eCO2 > 1000) {
      if (sgp_eCO2 > 2000) {
        spr.setTextColor(TFT_RED, TFT_BLACK);
      } else {
        spr.setTextColor(TFT_YELLOW, TFT_BLACK);
      }      
    } else {
      spr.setTextColor(TFT_WHITE, TFT_BLACK);
    }
    sprintf(buffer2, "eCO2 %d ppm  ", sgp_eCO2);
    tft.setCursor(0, 80); spr.printToSprite(buffer2);  
  }
  spr.unloadFont();
}




bool updateMQTT() {
  bool success = true;
  String baseTopic = MQTT_BASE_TOPIC + "/" + macAddr + "/";
 
  if (AM2322_success) {
    String topic = baseTopic + "temperature";
    String temperature = String(AM2322_temperature, 1);
    
    if (client.publish(topic.c_str(), temperature.c_str())) {
      Serial.println("Temperature Published");
    } else {
      success = false;
    }

    topic = baseTopic + "humidity";
    String humidity = String(AM2322_humidity, 1);

    if (client.publish(topic.c_str(), humidity.c_str())) {
      Serial.println("Humidity Published");
    } else {
      success = false;
    }
  } 

  if (sgp_success) {
    String topic = baseTopic + "TVOC";
    String tvoc = String(sgp_TVOC);
    
    if (client.publish(topic.c_str(), tvoc.c_str())) {
      Serial.println("TVOC Published");
    } else {
      success = false;
    }

    topic = baseTopic + "eCO2";
    String eco2 = String(sgp_eCO2);
    if (client.publish(topic.c_str(), eco2.c_str())) {
      Serial.println("eCO2 Published");
    } else {
      success = false;
    }
  } 
  
  if (mhz19_success) {    
    String topic = baseTopic + "CO2";
    String co2 = String(mhz19_co2);
    if (client.publish(topic.c_str(), co2.c_str())) {
      Serial.println("CO2 Published");
    } else {
      success = false;
    }

    String temp_mhz19 = String(mhz19_temperature);
    topic = baseTopic + "CO2-temperature";
    if (client.publish(topic.c_str(), temp_mhz19.c_str())) {
      Serial.println("MH-Z19C Temperature Published");
    } else {
      success = false;
    }
  }
  
  return(success);
}


bool connectMqtt() {
  Serial.println("Connecting to MQTT...");
  // tft.print("Connecting to MQTT");
  client.setServer(MQTT_HOST, MQTT_PORT);

  if (!client.connected()) {
    // tft.print(".");
    if (!client.connect(MQTT_CLIENTID, MQTT_USERNAME, MQTT_PASSWORD)) {
      Serial.print("MQTT connection failed:");
      Serial.print(client.state());
      return(false);
    }
  }
  // tft.println("");
  Serial.println("MQTT connected");
  // tft.println("MQTT connected");
  Serial.println("");
  return(true);
}

void disconnectMqtt() {
  client.disconnect();
  Serial.println("MQTT disconnected");
}

void checkWifiAndSubmitMQTT(void * parameter){ 
  vTaskDelay(5000 / portTICK_PERIOD_MS);
  
  for(;;){ // infinite loop
    int i = 0;
    while (WiFi.status() != WL_CONNECTED) {
      if (i == 0) {
        WiFi.disconnect();
        WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
      }
      vTaskDelay(1000 / portTICK_PERIOD_MS);
      Serial.print(".");
      i++;

      if (i == 30) {
        i = 0;
      }
    }

    Serial.print("Wifi connected.");

    if (connectMqtt()) {
      xSemaphoreTake( xMutex, portMAX_DELAY );
      updateMQTT();
      xSemaphoreGive( xMutex );
    }
    
    disconnectMqtt();
    // Pause the task for 10000ms
    vTaskDelay(60000 / portTICK_PERIOD_MS);   

  }
}
